from flask import Flask, flash, redirect,url_for, render_template, request, session, abort, make_response
import metapy
import pymongo
import pprint
from pymongo import MongoClient
from django.utils.encoding import smart_str, smart_unicode
from random import randint
 
app = Flask(__name__)

#This function return the classification of the specified recipe document
def classifier(document):
	#create forward index
	fidx = metapy.index.make_forward_index('classifierconfig.toml')
	
	dset = metapy.classify.MulticlassDataset(fidx)
	view = metapy.classify.MulticlassDatasetView(dset)
	view.shuffle()
	
	#create instance of a classifier with the forward index
	ova = metapy.classify.OneVsAll(view, metapy.classify.SGD, loss_id='hinge')
	
	#create a query documentnt from the query string
	doc = metapy.index.Document()
	doc.content(document)
	
	#return classification of the query document
	return ova.classify(fidx.tokenize(doc))

#This function returns all documents from the mongodb database that have the specified category
def searchcategory(category):
	client = MongoClient('localhost', 27017)
	db = client.tastyNLP
	posts = db.posts
	result = posts.find({'category':category})
	results=""
	print  posts.find({'category':'Seafood'}).count()
	for doc in result:
		results = results + "<hr><u style='font-weight: bolder'>Post</u>"+"<br><br>" 	
		results=results + "<h6>"+doc["message"]+"</h6>" + "<br>"
		if "comments" in doc:
			results = results + "<u style='font-weight: bolder' >Comments</u><br><br>" 	
			for comment in doc["comments"]["data"]:
				
				if len(comment["message"].strip().replace(".",""))>0:
					results = results +"<div style='margin-left:30px'>"+comment["message"] +"-" + comment["from"]["name"] + "</div>"

	return results.replace("\n","<br>")

#This function 	
def searchengine(querytxt):
	idx = metapy.index.make_inverted_index('searchconfig.toml')
	ranker = metapy.index.OkapiBM25()
	query = metapy.index.Document()
	query.content(querytxt)
	top_docs = ranker.score(idx, query, num_results=20)
	
	client = MongoClient('localhost', 27017)
	db = client.tastyNLP
	posts = db.posts

	results=""
	count = 0
	for num, (d_id, _) in enumerate(top_docs):
		content = idx.metadata(d_id).get('content')
		original_id=idx.metadata(d_id).get('path').strip()
		result = db.posts.find({'id':smart_str(original_id)})
			
		for doc in result:
			count = count + 1	
			results = results + "<hr><u style='font-weight: bolder'>Result "+str(count)+"</u><br><br>Category : "+doc["category"]+ "<br>Group : "+doc["groupname"]+"<br>Posted by : "+doc["from"]["name"]+"<br><br>"
			
			results=results + "<h6>"+doc["message"]+"</h6>" + "<br>"
			if "comments" in doc:
				results = results + "<u style='font-weight: bolder' >Comments</u><br><br>" 	
				for comment in doc["comments"]["data"]:
					
					if len(comment["message"].strip().replace(".",""))>0:
						results = results +"<div style='margin-left:30px'>"+comment["message"] +"-" + comment["from"]["name"] + "</div>"
	
	return results.replace("\n","<br>")


@app.route('/')
def index():
	return render_template("tastyNLP.html",**locals())
	
@app.route('/classify',methods = ['POST', 'GET'])
def classify():
	if request.method == 'POST':
		classifytext = request.form['txtrecipe'] 
		recipe = "Recipe: " + classifytext
		if len(classifytext) == 0:
			result="No recipe submitted"
		else:
			result = "Recipe classified under " +  classifier(classifytext)+ "."
	return render_template("classify.html",**locals())
	

@app.route('/tastyNLP/<category>',methods = ['POST', 'GET'])
def getallcategory(category):
	
	if request.method == 'GET':
		result = searchcategory(category)
	
	return render_template("tastyNLP.html",**locals())
		
		
@app.route('/tastyNLP',methods = ['POST', 'GET'])
def login():
	result=""
	if request.method == 'POST':
		searchtext = request.form['txtsearch']
		if len(searchtext) == 0:
			result = "Please enter a search query"
		else:
			result1 = searchengine(searchtext)
			response = make_response(result1)
			response.headers["content-type"]="text/plain"
			result = result1

	return render_template("tastyNLP.html",**locals())

if __name__ == "__main__":
    app.run(debug = False)