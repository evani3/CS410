# tastyNLP

A video presentation of this project can be found at: https://mediaspace.illinois.edu/media/t/1_208icy21


## INTRODUCTION
tastyNLP was born from a need to access and search for recipes posted on my favorite South African recipe-sharing Facebook groups. South African cooking is unique in that it combines Indian, Malay, African, Dutch and French cuisines. These hand-me-down recipes and tips aren’t typically available elsewhere. 
This document will describe the process of data gathering, cleansing, modeling and developing the web application where the information is finally presented.

## DATA GATHERING AND CLEANSING
There were two streams for data gathering; one for the text classifier and the other for document ranking.

_Classifier Data_**
For the text classifier I used a dataset that consisted of on a recipe title and ingredients. I did not think a method for each recipe would greatly improve the dataset in order to effective classify recipes as the  ingredients essentially classify a recipe according to the protein or non-protein categories. I made various calls to The Meal DB’s api and specified various search strings that corresponded the classifications I was interested in for example, chicken, beef, pork and vegetarian.
Below is an example of a call made to the api.
http://www.themealdb.com/api/json/v1/1/search.php?s=chicken

This process concluded with a classifier dataset consisting of 1225 labeled documents in the categories of chicken, beef, pork, seafood, vegetarian and sweets treats. The dataset and accompanying label and metadata files are found in the “classifierrecipes” folder within the project folder.

_Recipe Ranker Data_**
For the ranker, I initially planned to scrape data from a closed Facebook group but soon found that special permission was required from the group’s admin in order to get an access token to query data from the group. I decided instead to focus my data gathering on open Facebook groups. Data was extracted using Fabeook’s GraphAPI. An app token was generated from a Facebook app and extraction code was written in python using the Facebook python package. The extracted data was stored in mongodb. Please refer to the ipython notebook loadMongo.ipynb.

Data was collected from the four groups below:
1.	RECIPIES FOR AFRICA - SHARE your Favourite Recipes
2.	Strictly vegetarian recipes.
3.	Cooking Pot...South African Recipes
4.	Dump and Push Start" Easy Instant Pot Recipe

```python

import facebook
import metapy
import urllib
import io
import json
import pymongo
import re
from pymongo import MongoClient
from django.utils.encoding import smart_str, smart_unicode

def truncatecollection():
    #Connect to instance
    client = MongoClient('localhost', 27017)
    #Getting a Database
    db = client.tastyNLP
    #drop collection "posts"
    db.posts.drop()

def classifier(document):
    fidx = metapy.index.make_forward_index('classifierconfig.toml')
    dset = metapy.classify.MulticlassDataset(fidx)
    view = metapy.classify.MulticlassDatasetView(dset)
    view.shuffle()
    ova = metapy.classify.OneVsAll(view, metapy.classify.SGD, loss_id='hinge')
    doc = metapy.index.Document()
    doc.content(document)
    return ova.classify(fidx.tokenize(doc))

def getfacebookgroupdata(groupid,groupname,recordlimit):
    #Fetch data from facebook graph API
    my_token="EAACEdEose0cBAIlfron8oNN7kGPAdZCTGLZAeHpAROBiVZBInHpODbBZClZBmXAy8ZC0rGnZCBXlOpPRTCvWReB9KuEFcAZC7ZADTtG173eIgwjgLM44ay0JgwQbGigpXy8pZCz6YA1pcNvxZAx5hIEpiNQf063PfpaZAPZBxuhYEFZAUoHXnKXDfpFvxH8jSynybrUukZD"
    graph = facebook.GraphAPI(access_token=my_token,version='2.7')
    events = graph.request(groupid+"/feed?fields=from,message,comments,likes&limit="+recordlimit)
   
    client = MongoClient('localhost', 27017)
    db = client.tastyNLP
    posts = db.posts

    if isinstance(events, (dict)):
        for level in events:
            if isinstance(events[level],list):
                if "data" in events:
                    for data in events["data"]:
                        #Include posts where messages are recipes or questions.
                        if "message" in data and len(data["message"]) > 50: 
                            data["message"]=smart_str(data["message"])
                            data["groupname"]=groupname
                            data["category"]=""
                            posts.insert_one(data)

#Truncate the database and start collection of data from various open facebook groups 
truncatecollection()                            
getfacebookgroupdata("1645234332364979","RECIPIES FOR AFRICA - SHARE your Favourite Recipes","500")
getfacebookgroupdata("704989366257912","Strictly vegetarian recipes.","500")
getfacebookgroupdata("376854829180202","Cooking Pot...South African Recipes","500")
getfacebookgroupdata("502780413387424",'"Dump and Push Start" Easy Instant Pot Recipes',"500")

```

Due to the nature of Facebook group data, recipes can be provided in both message and comment posts. Also there are a lot of messages and comments that don’t contain recipes, so I excluded messages that were less then 100 characters and comments which were less than 200 characters. This seemed to zone in on posts where either recipes where shared in the messages or where recipes were asked for and provided by other users in the comments.
After the dataset was filtered and stored in mongodb, each relevant document was further enrichment by means of automatic classification using the classifier trained from the data specified in the previous section.


```python
recs={}

#Select the list of relevant documents based on length of message and comment. Update the category of the doc.
for doc in db.posts.find():
    if "message" in doc:
        x=(doc["message"].strip())
        #Choose length 100 to cater for relevant requests for recipes.
        if len(x)> 100:
            y=(x[0:30])
            clsfd = classifier(x)
            new_recipe = {"original_id": doc["id"],"recipe":x,"category":clsfd}
            #Update the post with the recipe classification
            posts.update_one({'id': doc["id"]},{'$set': { 'category': clsfd}}, upsert=True)
            recs[y]=new_recipe
            

            if "comments" in doc:
                new_recipe={}
                for comment in doc["comments"]["data"]:
                    x=(comment["message"].strip())
                    #Choose length 200 to cater for relevant recipes provided when the message was a recipes request
                    if len(x)> 200:
                        y=(x[0:30])
                        new_recipe = {"original_id": doc["id"],"recipe":x,"category":classifier(x)}
                        recs[y]=new_recipe
    
#Create dataset, label and metadata files
with io.open('C:/Project/searchrecipes/searchrecipes.dat.labels', 'w', encoding='utf-8') as fl:
    with io.open('C:/Project/searchrecipes/searchrecipes.dat', 'w', encoding='utf-8') as fr:
        with io.open('C:/Project/searchrecipes/metadata.dat', 'w', encoding='utf-8') as fm:
            for key in recs:
                fl.write("%s\n" % recs[key]["category"])
                fr.write("%s\n" % recs[key]["recipe"].replace('\n', ' ').replace('\t',' ').replace('\r',' '))
                fm.write("%s\n" % recs[key]["original_id"])
```

The dataset, label and metadata files were then created and saved to the “searchrecipes” folder to be indexed and used by the ranker. This process concluded with a dataset consisting of 867 recipe documents.

## WEB APPLICATION
Flask was used to create the web based application. The app.py file contains the relevant code which creates the indexes, classifier and search engine. 
Metapy was the tool of choice for both classifier and ranker. For the given dataset OkapiBM25 and Multiclass Classification: One-vs-all with Stochastic Gradient Decent performed the best.

## INSTRUCTIONS TO STARTING AND USING THE APPLICATION.
The application was developed using python and mongodb and both will need to be installed first.

### MONGODB
1.	Install mongodb

2.	Via the command line, navigate to the installation’s bin directory. 
    In my case this is `C:\Program Files\MongoDB\Server\3.6\bin`

3.	Restore the mongodb database called "tastyNLP"
    `mongorestore --drop -d tastyNLP <directory-of-downloaded-dump-backup eg dump\tastyNLP>`

4.	Start mongodb with the following command specifying the path to the data directory containing the db and log files.

    `mongod.exe --dbpath <path to data folder containing the restored "tastyNLP" database>`
    
### PYTHON DEPENDANCIES
The following python packages are required to be installed before the applicaiton is run.
1.	flask
2.	metapy
3.	pymongo
4.	facebook
5.	urllib
6.	io
7.	json
8.	pymongo
9.	re


### STARTING THE FLASK WEB APP
On ananconda prompt, navigate to the location of app.py and start the flask application server by doing the following: 
    `cd C:\Project`
    `python app.py`
    
To launch the web application, on a web browser (works best with Chrome) go to the following URL: 
[http://localhost:5000/tastyNLP](http://localhost:5000/tastyNLP)

### USING THE WEB APPLICATION
On the navigation bar you will find quick links to search recipes by classification. 

You can enter a query to search for recipes, for example "Chocolate"

You can also test the classifier by going to the "classifier" page and entering a recipe to test the classifier.
Copy the following recipe and paste in the text box and click search.

`Spicy Wraps
Ingredients(6)
2 packs of 6 SPAR Freshline wraps
10 ml SPAR olive oil
1 large red onion
2 medium red chillies
2 large cloves garlic
1 pack SPAR Butchers Best Cheese Grillers with Onion, roughly sliced
4 medium SPAR Freshline potatoes, unpeeled and cubed
salt and freshly ground black pepper
1 bunch fresh coriander leaves (dhania)
Method
Boil the potatoes in salted boiling water until very tender. Drain off the cooking water.
Heat the oil and fry the onion, chilli and garlic together, adding in the sausage slices after 2 minutes. Toss over the heat.
Using a potato masher, crush, as opposed to mash the drained cooked potatoes. Add to the pan, season to taste and add the coriander, turning and cooking until well heated through and the potato creates small pieces of crust on the pan-base to be incorporated.
 Warm the wraps as per the package instructions.
Serve the filling with the wraps, for guests to spoon for themselves. The best way is to follow these 3 steps.
Makes 12 wraps (allow 2-3 per person)`







